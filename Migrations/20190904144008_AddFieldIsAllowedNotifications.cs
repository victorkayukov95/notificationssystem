﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificationsSystem.Migrations
{
    public partial class AddFieldIsAllowedNotifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAllowedNotifications",
                table: "Users",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAllowedNotifications",
                table: "Users");
        }
    }
}
