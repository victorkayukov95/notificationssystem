﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NotificationsSystem.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Managers { get; set; }
        public string SpreadsheetId { get; set; }
        public bool IsAllowedNotifications { get; set; }
    }
}
