﻿using Microsoft.EntityFrameworkCore;
using NotificationsSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationsSystem.Data
{
    public class NotificationsSystemContext : DbContext
    {
        public NotificationsSystemContext(DbContextOptions<NotificationsSystemContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
    }
}
