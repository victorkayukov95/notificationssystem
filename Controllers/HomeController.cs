﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotificationsSystem.Repositories;
using System;

namespace NotificationsSystem.Controllers
{
    [Authorize(AuthenticationSchemes = "Application")]
    public class HomeController : BaseController
    {
        public HomeController(IUserRepository userRepository) 
            : base(userRepository)
        {
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                userRepository.CheckEmail(this.CurrentEmail);
                return View("Index", this.CurrentName.Split(' ')[0]);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }
    }
}
