﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotificationsSystem.Helpers;
using NotificationsSystem.Repositories;
using System;

namespace NotificationsSystem.Controllers
{
    [Authorize(AuthenticationSchemes = "Application")]
    public class SettingsController : BaseController
    {
        public SettingsController(IUserRepository userRepository) 
            : base(userRepository)
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                ViewBag.Title = "Settings";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }

        [HttpGet]
        public IActionResult UserInfo()
        {
            try
            {
                return View(userRepository.GetUserInfo(this.CurrentEmail));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }

        [HttpPost]
        public IActionResult UserInfo(UserInfo userInfo)
        {
            try
            {
                userRepository.UpdateUserInfo(userInfo);
                return Ok();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }
    }
}
