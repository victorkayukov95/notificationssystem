﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotificationsSystem.Repositories;
using System;

namespace NotificationsSystem.Controllers
{
    [Authorize(AuthenticationSchemes = "Application")]
    public class ManagersController : BaseController
    {
        public ManagersController(IUserRepository userRepository)
             : base(userRepository)
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                ViewBag.Title = "Managers";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }

        [HttpPost]
        public IActionResult ManagersList()
        {
            try
            {
                return View(userRepository.GetManagersList(this.CurrentEmail));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }

        [HttpPost]
        public IActionResult Manager(string newManagerEmail)
        {
            try
            {
                userRepository.AddManager(this.CurrentEmail, newManagerEmail);
                return Ok();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }

        [HttpPost]
        public IActionResult DeleteManager(string managerEmail)
        {
            try
            {
                userRepository.DeleteManager(this.CurrentEmail, managerEmail);
                return Ok();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }
    }
}
