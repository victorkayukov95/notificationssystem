﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NotificationsSystem.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login()
        {
            ViewBag.Title = "Login";
            return View();
        }

        public IActionResult GoogleLogin(string returnUrl)
        {
            return new ChallengeResult(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties
            {
                RedirectUri = Url.Action(nameof(LoginCallback), new { returnUrl })
            });
        }

        public async Task<IActionResult> LoginCallback(string returnUrl)
        {
            var authenticateResult = await HttpContext.AuthenticateAsync("External");
            if (!authenticateResult.Succeeded)
                return BadRequest();

            var claimsIdentity = new ClaimsIdentity("Application");

            claimsIdentity.AddClaim(authenticateResult.Principal.FindFirst(ClaimTypes.NameIdentifier));
            claimsIdentity.AddClaim(authenticateResult.Principal.FindFirst(ClaimTypes.Email));
            claimsIdentity.AddClaim(authenticateResult.Principal.FindFirst(ClaimTypes.Name));

            await HttpContext.SignOutAsync("External");
            await HttpContext.SignInAsync("Application", new ClaimsPrincipal(claimsIdentity));

            return LocalRedirect(returnUrl);
        }

        public async Task<IActionResult> GoogleLogout()
        {
            await HttpContext.SignOutAsync("Application");
            return RedirectToAction("Login");
        }        
    }
}