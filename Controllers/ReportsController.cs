﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotificationsSystem.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NotificationsSystem.Controllers
{
    [Authorize(AuthenticationSchemes = "Application")]
    public class ReportsController : BaseController
    {
        public ReportsController(IUserRepository userRepository)
            : base(userRepository)
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                return View("Index", this.GetAllSheets());
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
            
        }

        [HttpPost]
        public IActionResult TasksList(string filter = null)
        {
            try
            {
                ViewBag.Filter = filter;
                return View(this.GetAllTasks(filter));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }

        [HttpPost]
        public IActionResult Task(string range, IList<string> row)
        {
            try
            {
                var task = (IList<object>)row.Cast<object>().ToList();
                this.AddTask(range, new List<IList<object>>() { task });
                return Ok();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("ErrorPage");
            }
        }       
    }
}