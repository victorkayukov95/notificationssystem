﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Microsoft.AspNetCore.Mvc;
using NotificationsSystem.Helpers;
using NotificationsSystem.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;

namespace NotificationsSystem.Controllers
{
    public class BaseController : Controller
    {
        public readonly IUserRepository userRepository;

        public BaseController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public string CurrentEmail => HttpContext?.User?.FindFirst(ClaimTypes.Email)?.Value ?? string.Empty;
        public string CurrentName => HttpContext?.User?.FindFirst(ClaimTypes.Name)?.Value ?? string.Empty;

        #region CRUD
        public Tasks GetAllTasks(string filter = null)
        {
            string spreadsheetId = userRepository.GetSpreadsheetIdByEmail(CurrentEmail);
            string sheet = filter == null ? DateTime.Now.ToString("MMMM") : filter;
            var sheetsService = GetSheetsService();
            var isExistingSheet = sheetsService.Spreadsheets.Get(spreadsheetId).Execute().Sheets.Any(x => x.Properties.Title == sheet);
            if (isExistingSheet)
            {
                var existingSpreadsheets = sheetsService.Spreadsheets.Values.Get(spreadsheetId, $"{sheet}!A:G").Execute();
                if (existingSpreadsheets.Values == null)
                {
                    var reportedTaskTemplate = new List<IList<object>> { new List<object> { "Date", "Start Time", "Spend Time (h)", "Description" } };
                    this.AddTask("A1:D1", reportedTaskTemplate);
                    var evaluatedTaskTemplate = new List<IList<object>> { new List<object> { "Task #", "Task Name", "Estimate (h)" } };
                    this.AddTask("A33:D33", evaluatedTaskTemplate);
                }

                return new Tasks
                {
                    ReportedTasks = sheetsService.Spreadsheets.Values.Get(spreadsheetId, $"{sheet}!A1:G32").Execute().Values,
                    EvaluatedTasks = sheetsService.Spreadsheets.Values.Get(spreadsheetId, $"{sheet}!A33:D").Execute().Values
                };
            }
            return new Tasks();
        }

        public void AddTask(string range, IList<IList<object>> task)
        {
            string spreadsheetId = userRepository.GetSpreadsheetIdByEmail(CurrentEmail);

            SpreadsheetsResource.ValuesResource.AppendRequest request =
                       GetSheetsService().Spreadsheets.Values.Append(new ValueRange() { Values = task }, spreadsheetId, $"{DateTime.Now.ToString("MMMM")}!{range}");
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.OVERWRITE;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.RAW;
            request.Execute();
        }

        public IList<string> GetAllSheets()
        {
            var sheetsService = this.GetSheetsService();
            string spreadsheetId = userRepository.GetSpreadsheetIdByEmail(CurrentEmail);
            return sheetsService.Spreadsheets.Get(spreadsheetId).Execute().Sheets.Select(x => x.Properties.Title).ToList();
        }

        #endregion

        #region Private Methods
        private SheetsService GetSheetsService()
        {
            string[] scopes = { SheetsService.Scope.Spreadsheets };
            string applicationName = "Notifications System";
            UserCredential credential;
            using (var stream = new FileStream("credential.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    "user",
                    CancellationToken.None).Result;
            }

            return new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName,
            });
        }
        #endregion
    }
}
