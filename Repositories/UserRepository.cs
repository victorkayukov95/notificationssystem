﻿using NotificationsSystem.Data;
using NotificationsSystem.Helpers;
using NotificationsSystem.Models;
using System.Collections.Generic;
using System.Linq;

namespace NotificationsSystem.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly NotificationsSystemContext context;

        public UserRepository(NotificationsSystemContext context)
        {
            this.context = context;
        }

        public void CheckEmail(string currentEmail)
        {
            if (!string.IsNullOrEmpty(currentEmail))
            {
                var user = this.GetUser(currentEmail);
                if (user == null)
                {
                    context.Users.Add(new User { Email = currentEmail, SpreadsheetId = "12j6kh7sfoOMXGkuqEOqHVJPd-cJUVDrDPlJ3keAyD2A" });
                    context.SaveChanges();
                }
            }
        }

        public string GetSpreadsheetIdByEmail(string currentEmail)
        {
            var user = this.GetUser(currentEmail);
            return user != null ? user.SpreadsheetId : string.Empty;
        }

        public void AddManager(string currentEmail, string newManagerEmail)
        {
            var user = this.GetUser(currentEmail);
            if (user != null)
            {
                if (string.IsNullOrEmpty(user.Managers))
                {
                    user.Managers = $"{newManagerEmail} ,";
                }
                else if(!user.Managers.Contains(newManagerEmail))
                {
                    user.Managers += $"{newManagerEmail} ,";
                }
                
                context.SaveChanges();
            }
        }

        public void DeleteManager(string currentEmail, string managerEmail)
        {
            var user = this.GetUser(currentEmail);
            if (user != null) 
            {
                if (user.Managers.Contains(managerEmail))
                {
                    var managers = user.Managers.Split(',').Where(x => x != managerEmail);
                    user.Managers = string.Join(',', managers);
                    context.SaveChanges();
                }
            }
        }

        public void UpdateUserInfo(UserInfo userInfo)
        {
            var user = this.GetUser(userInfo.Email);
            if (user != null)
            {
                if (user.SpreadsheetId != userInfo.SpreadsheetId)
                {
                    user.SpreadsheetId = userInfo.SpreadsheetId;
                }
                if (user.IsAllowedNotifications != userInfo.IsAllowedNotifications)
                {
                    user.IsAllowedNotifications = userInfo.IsAllowedNotifications;
                }
                context.SaveChanges();
            }
        }

        public UserInfo GetUserInfo(string currentEmail)
        {
            var user = this.GetUser(currentEmail);
            if (user != null)
            {
                return new UserInfo
                {
                    Email = user.Email,
                    SpreadsheetId = user.SpreadsheetId,
                    IsAllowedNotifications = user.IsAllowedNotifications
                };
            }
            return new UserInfo();
        }
       
        public IList<string> GetManagersList(string currentEmail)
        {
            var user = this.GetUser(currentEmail);
            if (user != null)
            {
                return user.Managers?.Split(',').Where(x => x != string.Empty).ToList<string>() ?? new List<string>();
            }
            return new List<string>();
        }

        #region Private Methods
        private User GetUser(string currentEmail) => !string.IsNullOrEmpty(currentEmail) ? context.Users.FirstOrDefault(x => x.Email == currentEmail) : null;
        #endregion
    }
}
