﻿using NotificationsSystem.Helpers;
using System.Collections.Generic;

namespace NotificationsSystem.Repositories
{
    public interface IUserRepository
    {
        void CheckEmail(string currentEmail);
        string GetSpreadsheetIdByEmail(string currentEmail);
        void AddManager(string currentEmail, string newManagerEmail);
        void DeleteManager(string currentEmail, string managerEmail);
        void UpdateUserInfo(UserInfo userInfo);
        UserInfo GetUserInfo(string currentEmail);
        IList<string> GetManagersList(string currentEmail);
    }
}
