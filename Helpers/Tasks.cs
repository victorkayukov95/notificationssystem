﻿using System.Collections.Generic;

namespace NotificationsSystem.Helpers
{
    public class Tasks
    {
        public IList<IList<object>> ReportedTasks { get; set; }
        public IList<IList<object>> EvaluatedTasks { get; set; }
    }
}
