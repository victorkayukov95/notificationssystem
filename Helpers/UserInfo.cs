﻿namespace NotificationsSystem.Helpers
{
    public class UserInfo
    {
        public string Email { get; set; }
        public string SpreadsheetId { get; set; }
        public bool IsAllowedNotifications { get; set; }
    }
}
